# RealtimeChat

Real Time chat Menggunakan Socket.Io, html dan Javascript

Apa itu Socket.io? silakan pelajari terlebih dahulu di [](https://socket.io/).
Socket.io merupakan library untuk Realtime data, komunikasi Realtime 2 arah dan bisa digunakan di berbagai platform.

# Install
Untuk install semua depedencies.

`npm i`

# Run socket.io
`npm run devStart`


# Run RealtimeChat

`silahkan eksekusi index.html`


